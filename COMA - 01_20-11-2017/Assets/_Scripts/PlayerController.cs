﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    [System.Serializable]
    public class MoveSettings
    {
        public float maxSpeed = 50f;
        public float acceleration = 20f;
        public float distToGrounded = 0.5f;
        public float jumpVel = 25;
        public LayerMask ground;
    }

    [System.Serializable]
    public class PhysSettings
    {
        public float downAccel = 0.75f;
    }

    [System.Serializable]
    public class InputSettings
    {
        public float inputDelay = 0.1f;
        public string FORWARD_AXIS = "Horizontal";
        public string JUMP_AXIS = "Jump";
    }

    public MoveSettings moveSettings = new MoveSettings();
    public PhysSettings physSettings = new PhysSettings();
    public InputSettings inputSettings = new InputSettings();

    public Vector3 velocity = Vector3.zero;
    private Rigidbody rBody;
    float horizontalInput, verticalInput, jumpInput;    

    bool Grounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, moveSettings.distToGrounded, moveSettings.ground);
    }

    private void Start()
    {
        if (GetComponent<Rigidbody>())
            rBody = GetComponent<Rigidbody>();
        else
            Debug.LogError("The character needs a rigidbody!");

        horizontalInput = verticalInput = jumpInput = 0;
    }

    private void Update()
    {
        GetInput();
        //Debug.Log("is grounded: " + Grounded());
    }

    private void FixedUpdate()
    {
        Run();
        Jump();

        rBody.velocity = transform.TransformDirection(velocity);
    }

    void GetInput()
    {
        horizontalInput = Input.GetAxis(inputSettings.FORWARD_AXIS);
        jumpInput = Input.GetAxisRaw(inputSettings.JUMP_AXIS);

    }

    void Run()
    {
        if (Mathf.Abs(horizontalInput) > inputSettings.inputDelay)
        {
            //rBody.velocity = transform.right * horizontalInput * moveSettings.acceleration;
            velocity.x = moveSettings.acceleration * horizontalInput;
        }
        else
            velocity.x = 0;
    }

    void Jump()
    {
        if (jumpInput > 0 && Grounded())
        {
            velocity.y = moveSettings.jumpVel;
        }
        else if (jumpInput == 0 && Grounded())
        {
            velocity.y = 0;
        }
        else
        {
            velocity.y -= physSettings.downAccel;
        }
    }
}
