﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform target;
    public float lookSmooth = 0.09f;
    public float lookSmoothStepBack = 2f;
    public float camStepBack = 1f;
    public Vector3 offsetFromTarget = new Vector3(0, 6, -14);
    public float xTilt = 10;
    public float camZdistance = 14f;

    Vector3 destination = Vector3.zero;
    PlayerController pController;
    float rotateVel = 0;

    private void Start()
    {
        SetCameraTarget(target);        
    }

    void SetCameraTarget(Transform t)
    {
        target = t;

        if (target != null)
        {
            if (target.GetComponent<PlayerController>())
            {
                pController = target.GetComponent<PlayerController>();
            }
            else
                Debug.LogError("The camera's target needs a character controller");
        }
        else
            Debug.LogError("Your camera needs a target");
    }

    // Maybe use LateUpdate, but so far it lags
    private void FixedUpdate()
    {
        MoveToTarget();
        //LookAtTarget();
    }

    void MoveToTarget()
    {
        //destination = pController.transform.position + offsetFromTarget;
        //destination = target.transform.position;
        float posX = Mathf.Lerp(transform.position.x, target.position.x, lookSmooth);
        float posY = Mathf.Lerp(transform.position.y, target.position.y + 2.02f, lookSmooth);
        float posZ;
        // Dopisz poniżej warunki na skakanie i bieganie w obie strony
        if (pController.velocity != Vector3.zero)
        {
            posZ = Mathf.Lerp(transform.position.z, (target.position.z - camZdistance) - camStepBack, Time.deltaTime * lookSmoothStepBack);
        }
        else
        {
            posZ = Mathf.Lerp(transform.position.z, target.position.z - camZdistance, Time.deltaTime * lookSmoothStepBack);
        }

        transform.position = new Vector3(posX, posY, posZ);
        //transform.position.x = Mathf.Lerp(transform.position.x, target.position.x, lookSmooth);
        

        // Camera movement while accelerating

    }

    void LookAtTarget()
    {
        //float eulerYAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y, target.eulerAngles.y, ref rotateVel, lookSmooth);
        //transform.rotation = Quaternion.Euler(transform.eulerAngles.x, eulerYAngle, 0);
    }

    void CameraMovAccel()
    {
        if (pController.velocity.x > 0)
        {
            float posZ = Mathf.Lerp(target.position.z - camZdistance, (target.position.z - camZdistance) - camStepBack, lookSmoothStepBack);
        }
        else
        {
            float posZ = Mathf.Lerp(transform.position.z, target.position.z - camZdistance, lookSmoothStepBack);
        }
    }
}